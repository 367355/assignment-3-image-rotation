#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "image.h"

bool rotate(struct image * img, int32_t angle);
