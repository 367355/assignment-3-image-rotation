#pragma once

#include <stdint.h>
#include <stdlib.h>

#include "pixel.h"

#define PIXEL_SIZE 3

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel *data;
};

void image_destroy(struct image source);
