#pragma once

#include <stdio.h>

#include "image.h"

enum bmp_read_status {
    BMP_READ_OK = 0,
    BMP_READ_INVALID_SIGNATURE,
    BMP_READ_INVALID_BITS,
    BMP_READ_INVALID_HEADER,
    BMP_READ_INVALID_COMPRESSION,
    BMP_READ_INVALID_FILE_POINTER,
    BMP_READ_INVALID_IMAGE,
};

enum bmp_read_status read_bmp(FILE* in, struct image* img);

enum bmp_write_status {
    BMP_WRITE_OK = 0,
    BMP_WRITE_ERROR,
    BMP_WRITE_HEADER_ERROR,
    BMP_WRITE_INVALID_FILE_POINTER
};

enum bmp_write_status write_bmp(FILE* out, struct image * img);

static char * const read_errors_messages[] = {
    [BMP_READ_INVALID_SIGNATURE] = "Error reading: The input file does not have the expected signature of a bmp file.",
    [BMP_READ_INVALID_BITS] = "Error reading: Only 24-bit bmp files are supported for reading.",
    [BMP_READ_INVALID_HEADER] = "Error reading: The input file header is invalid.",
    [BMP_READ_INVALID_COMPRESSION] = "Error reading: The compression attribute in the bmp file header has an invalid value.",
    [BMP_READ_INVALID_FILE_POINTER] = "Error reading: Failed to open the input file. It may not exist or there could be a file pointer issue.",
    [BMP_READ_INVALID_IMAGE] = "Error reading: The image array in the file is invalid, possibly due to corruption."
};

static char * const write_errors_messages[] = {
    [BMP_WRITE_ERROR] = "Error writing: Data could not be successfully written to the output file.",
    [BMP_WRITE_HEADER_ERROR] = "Error writing: An error occurred while writing the file header.",
    [BMP_WRITE_INVALID_FILE_POINTER] = "Error writing: Failed to open the output file for writing."
};
