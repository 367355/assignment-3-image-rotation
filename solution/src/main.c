#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "image.h"
#include "image_io.h"
#include "rotate.h"

int main( int argc, char** argv ) {
    if (argc != 4) {
        fprintf(stderr, "Checking error: expected 3 arguments\n");
        return 1;
    }

    const char * in_file_path = argv[1];
    const char * out_file_path = argv[2];
    const char * angle_str = argv[3];

    char *endptr;
    const int32_t angle = (int32_t)strtol(angle_str, &endptr, 10);
    if (*endptr != '\0') {
        fprintf(stderr, "Invalid angle: %s\n", angle_str);
        return 2;
    }

    FILE * in_file = fopen(in_file_path, "rb");

    struct image img = {0};

    enum bmp_read_status read_status = read_bmp(in_file, &img);

    if (fclose(in_file)) {
        fprintf(stderr, "Warning: Input file was not successfully closed\n");
    }

    if (read_status != BMP_READ_OK) {
        fprintf(stderr, "%s\n", read_errors_messages[read_status]);
        image_destroy(img);
        return 3;
    }

    FILE * out_file = fopen(out_file_path, "wb");

    if (!rotate(&img, angle)) {
        fprintf(stderr, "Invalid angle: %d, expected 0, 90, 180, 270, -90, -180 or -270\n", angle);
        return 4;
    }

    enum bmp_write_status write_status = write_bmp(out_file, &img);

    image_destroy(img);

    if (fclose(out_file)) {
        fprintf(stderr, "Error: output File was not successfully closed. Exit...\n");
        return 5;
    }

    if (write_status != BMP_WRITE_OK) {
        fprintf(stderr, "%s\n", write_errors_messages[write_status]);
        return 6;
    }

    return 0;
}
