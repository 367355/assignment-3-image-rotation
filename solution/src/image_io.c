#include <stdlib.h>

#include "image_io.h"
#include "bmp_header.h"

static enum bmp_read_status check_read_header(struct bmp_header const header) {
    if (header.file_type != BMP_HEADER_FILE_TYPE)           return BMP_READ_INVALID_SIGNATURE;
    if ((size_t) header.header_size != BMP_HEADER_SIZE)     return BMP_READ_INVALID_HEADER;
    if (header.compression != BMP_HEADER_COMPRESSION)       return BMP_READ_INVALID_COMPRESSION;
    if (header.planes != BMP_HEADER_PLANES)                 return BMP_READ_INVALID_HEADER;
    if (header.bpp != BMP_HEADER_BPP)                       return BMP_READ_INVALID_BITS;
    return BMP_READ_OK;
}

static uint8_t calc_padding(uint64_t width) {
    return ((PIXEL_SIZE * width) % 4 == 0) ? 0 : 4 - (PIXEL_SIZE * width) % 4;
}

enum bmp_read_status read_bmp(FILE* in, struct image* img) {
    if (in == NULL)
        return BMP_READ_INVALID_FILE_POINTER;

    struct bmp_header header = {0};

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return BMP_READ_INVALID_HEADER;
    }

    enum bmp_read_status check_read_header_result = check_read_header(header);
    if (check_read_header_result != BMP_READ_OK) {
        return check_read_header_result;
    }

    img->height = header.height;
    img->width  = header.width;
    img->data   = malloc(img->height * img->width * PIXEL_SIZE);

    uint8_t const padding = calc_padding(img->width);

    for (uint64_t i = img->height; i-- > 0 ;) {
        uint64_t bytes_read = fread((img->data + i * img->width), PIXEL_SIZE, img->width, in);

        if (bytes_read != img->width) {
            return BMP_READ_INVALID_IMAGE;
        }

        if (fseek(in, padding, SEEK_CUR) != 0) {
            return BMP_READ_INVALID_IMAGE;
        }
    }

    return BMP_READ_OK;
}

static struct bmp_header create_header(struct image const * img) {
    return (struct bmp_header) {
        .file_type = BMP_HEADER_FILE_TYPE,
        .file_size = img->height * (PIXEL_SIZE * img->width + calc_padding(img->width)) + BMP_HEADER_DATA_OFFSET,
        .reserved = 0x0,
        .data_offset = BMP_HEADER_DATA_OFFSET,
        .header_size = BMP_HEADER_SIZE,
        .width = img->width,
        .height = img->height,
        .planes = BMP_HEADER_PLANES,
        .bpp = BMP_HEADER_BPP,
        .compression = BMP_HEADER_COMPRESSION,
        .image_size = img->height * (PIXEL_SIZE * img->width + calc_padding(img->width)),
        .x_ppm = 0x0,
        .y_ppm = 0x0,
        .colors_used = 0x0,
        .colors_important = 0x0,
    };
}

enum bmp_write_status write_bmp(FILE* out, struct image* img) {
    if (out == NULL)
        return BMP_WRITE_INVALID_FILE_POINTER;

    struct bmp_header header = create_header(img);

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out))
        return BMP_WRITE_HEADER_ERROR;

    uint8_t const padding = calc_padding(img->width);
    uint32_t null_byte = 0;

    for (uint64_t i = img->height; i-- > 0 ;) {
        uint64_t written = fwrite((img->data + i * img->width), PIXEL_SIZE, img->width, out);
        written += fwrite(&null_byte, 1, padding, out);

        if (written != img->width + padding) {
            return BMP_WRITE_ERROR;
        }
    }

    return BMP_WRITE_OK;
}
