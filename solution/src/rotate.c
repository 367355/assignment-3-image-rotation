#include "rotate.h"
#include <stdint.h>
#include <stdio.h>

static void rotate90(struct image * img) {
    struct image rotate_img = {0};
    rotate_img.width = img->height;
    rotate_img.height = img->width;
    rotate_img.data   = malloc(img->height * img->width * PIXEL_SIZE);
    for (uint64_t i = 0; i < img->width; i++)
        for (uint64_t j = 0; j < img->height; j++) {
            //rotate_img.data[img->height-j-1 + img->height*i] = img->data[i + img->height*j];
            rotate_img.data[img->height * i + j] = img->data[i + img->width * (img->height - j - 1)];
        }
    free(img->data);
    *img = rotate_img;
}

static void rotate180(struct image * img) {
    uint64_t size = img->width * img->height;
    for (uint64_t pixel = 0; pixel < (size)/2; pixel++) {
        struct pixel swap = img->data[pixel];
        img->data[pixel] = img->data[size-pixel-1];
        img->data[size-pixel-1] = swap;
    }
}

static void rotate270(struct image * img) {
    rotate180(img);
    rotate90(img);
}

bool rotate(struct image * img, int32_t angle) {
    switch (angle) {
        case 0:
            break;
        case 90:
        case -270:
            rotate90(img);
            break;
        case -90:
        case 270:
            rotate270(img);
            break;
        case 180:
        case -180:
            rotate180(img);
            break;
        default:
            return false;
    };
    return true;
}
