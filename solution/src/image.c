#include "image.h"

void image_destroy(struct image source) {
    if (source.data != NULL)
        free(source.data);
}
